#!/usr/bin/python
# thePrompter
# Evan Krause
# Python 2.7
# Pyside 1.1.1

import sys
from PySide.QtCore import Qt
from PySide.QtGui import QTextEdit, QPushButton, QCheckBox, QColorDialog
from PySide.QtGui import QWidget, QMenuBar,  QMainWindow, QLineEdit, QSlider
from PySide.QtGui import QGraphicsView, QLabel, QVBoxLayout, QBoxLayout
from PySide.QtGui import QColor, QGraphicsScene, QGraphicsTextItem, QFormLayout
from PySide.QtGui import QListWidget, QFileDialog, QMatrix, QGraphicsRectItem
from PySide.QtGui import QGridLayout, QAction, QKeySequence, QDialog
from PySide.QtGui import QApplication, QPen, QMessageBox

__version__ = '0.23'

placeholder = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. \
    Aenean commodo ligula eget dolor. Aenean massa. \
    Cam sociis natoque penatibus et magnis dis parturient montes, \
    nascetur ridiculus mus. \
    Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. \
    Nulla consequat massa quis enim. \
    Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. \
    In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. \
    Nullam dictum felis eu pede mollis pretium. \
    Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. \
    Aenean vulputate eleifend"


allText = ""
enteredText = placeholder
tags = ""
endtags = ""

bgColor = QColor.fromHsl(0,  0, 0)  # black
fgColor = QColor.fromHsl(0,  0, 255)  # white

defaultcolor = "FFFFFF"
defaultjust = "center"
defaultsize = 32
defaultface = "Consolas, Menlo, Courier, Verdana, Helvetica"
defaultbold = True
defaultital = False
defaultund = False

defaultnewtitle = "Title"
defaultnewtext = "Insert text here..."

settingsWindow = None


def trunc(str, len):
    """ truncates a string to a given length
    if it already shorter, just returns the string
    otherwise, cuts of 3 characters and adds '...' """
    if str.__len__() <= len - 1:
        return str
    else:
        return str[:len - 3] + "..."


class Options:
    def __init__(self):
        """ holds all options the user can configure """
        self.color = defaultcolor
        self.just = defaultjust
        self.size = defaultsize
        self.face = defaultface
        self.bold = defaultbold
        self.ital = defaultital
        self.und = defaultund
        self.hflip = True
        self.vflip = False
        self.zoomX = 1.0
        self.zoomY = 1.0
        self.margins = 50
        self.vmarginMult = 0.3
        self.dx = 0
        self.dy = 0
        self.width = 960
        self.height = 720
        self.updateOnEdit = True
        self.viewHtml = False
        self.showProgressIndicator = True
        self.showTitle = True


class Slides:
    """ holds all slides text, titles, QGScenes, and QGTextItems,
        as well as methods to manipulate slides """
    def __init__(self):
        self.width = 2000
        self.height = 2000
        self.titles = []  # holds slide titles
        self.text = []    # holds slide content
        self.slides = []  # holds QGraphicsTextItem
        self.scenes = []  # holds QGraphicsScene
        self.active = -1
        self.blankscene = QGraphicsScene()
        self.nullObject = None
        self.createNull()

    def new(self, title, text):
        """ creates a new slide with given text and title
            sets basic options like bg color, text width (for wrapping)
            also creates text, title, slide, scene list entries
            and modifies active slide """
        if self.isslide(self.active) is False and self.active >= 0:
            print "Woah! trying to make a new slide next to a nonexistant one!"
        self.active += 1
        a = self.active
        self.scenes.insert(a, QGraphicsScene())
        self.slides.insert(a, QGraphicsTextItem())
        self.text.insert(a, text)
        self.titles.insert(a, title)
        self.scenes[a].setSceneRect(0, 0, options.width, options.height)
        self.scenes[a].setBackgroundBrush(bgColor)
        self.slides[a].setTextWidth(options.width)
        self.slides[a].setDefaultTextColor(fgColor)
        self.slides[a].setHtml(text)
        self.createNull()
        self.nullObject.setPen(QPen(Qt.NoPen))
        self.scenes[a].addItem(self.nullObject)
        self.scenes[a].addItem(self.slides[a])
        controller.edit.setText(text)
        controller.title.setText(title)
        controller.slidelist.insertItem(a + 1, "")
        controller.updateList(title, text)

    def duplicate(self):
        """ duplicates current slide """
        text = controller.edit.toPlainText()
        title = controller.title.text()
        slides.new(title, text)

    def length(self):
        """ returns length of list """
        return self.text.__len__()

    def ilength(self):
        """ returns length of list - 1
            for use in iterating from 0th element to the last """
        return self.text.__len__() - 1

    def createNull(self):
        """ create object used to ensure all slides are scaled the same way
            this prevents bad aspect ratios and scaling based on length of text
        """
        w, h = options.width, options.height
        self.nullObject = QGraphicsRectItem(0, 0, w, h)

    def delete(self, slide):
        """ tries to delete given slide. will always leave 1 slide left
            even though having no slides is supported... """
        update()  # ensure list is up to date
        self.prevSlide()
        if self.length() <= 1:
            print "must have 1 slide!"
            return
        try:
            self.text.pop(slide)
            self.titles.pop(slide)
            self.slides.pop(slide)
            self.scenes.pop(slide)
        except IndexError:
            print "error deleting slide! index error"

    def move(self, slide, position):
        """ untested and unused. OH WELL """
        self.text.insert(position - 1, self.text.pop(slide))
        print ""

    def changeActive(self, slide):
        """ slides.changeActive(slide) handles switching slides.
            nextSlide(), prevSlide() etc just call this function """
        self.active = slide
        options.updateOnEdit = False
        # prevents next 3 lines from updating and overwriting titles
        controller.slidelist.setCurrentRow(self.active)
        controller.edit.setText(self.text[self.active])
        controller.title.setText(self.titles[self.active])
        options.updateOnEdit = True
        slidenum = str(self.active + 1)
        total = str(len(self.slides))
        controller.slidelabel.setText("Slide: %s of %s" % (slidenum, total))
        update()

    def nextSlide(self):
        """ changes to next slide, if possible """
        if self.active + 1 > self.slides.__len__() - 1:
            return
        self.changeActive(self.active + 1)

    def prevSlide(self):
        """ changes to previous slide, if possible """
        if self.active - 1 < 0:
            return
        self.changeActive(self.active - 1)

    def setFirst(self):
        """ changes to First slide, if possible """
        self.changeActive(0)

    def setLast(self):
        """ changes to last slide, if possible """
        self.changeActive(self.ilength())

    def currentScene(self):
        """ returns active QGraphicsScene unless it doesn't exist """
        try:
            return self.scenes[self.active]
        except:
            return self.blankscene

    def isslide(self, slide):
        """ checks if a given slide exists
        i.e., the arrays holding slide information contains this element """
        try:
            self.text[slide] != None
        except ValueError:
            return False
        except IndexError:
            return False
        return True

    def delActive(self):
        """ deletes current slide """
        self.delete(self.active - 1)
        update()

    def setCurrentText(self, title, text, html):
        """ saves title and text, sets full html output for display """
        self.titles[self.active] = title
        self.text[self.active] = text
        if options.viewHtml == False:  # mostly for debugging...
            self.slides[self.active].setHtml(html)
        else:
            self.slides[self.active].setPlainText(html)


class Menu:
    """ creates menu bar with options like new slide
        new document, save, open, etc.. """
    def __init__(self):
        self.menu = QMenuBar()
        self.setActions()
        self.createMenus()

    def createMenus(self):
        """ adds menu options/ groups """
        fileMenu = self.menu.addMenu("File")
        slidesMenu = self.menu.addMenu("Slides")
        styleMenu = self.menu.addMenu("Style")
        helpMenu = self.menu.addMenu("Help")

        fileMenu.addAction(self.newAct)
        fileMenu.addAction(self.openAct)
        fileMenu.addSeparator()
        fileMenu.addAction(self.saveAct)
        fileMenu.addSeparator()
        aMenu = fileMenu.addMenu(u'\u2191')
        bMenu = aMenu.addMenu(u'\u2191')
        cMenu = bMenu.addMenu(u'\u2193')
        dMenu = cMenu.addMenu(u'\u2193')
        eMenu = dMenu.addMenu(u'\u2190')
        fMenu = eMenu.addMenu(u'\u2192')
        gMenu = fMenu.addMenu(u'\u2190')
        hMenu = gMenu.addMenu(u'\u2192')
        iMenu = hMenu.addMenu('B')
        jMenu = iMenu.addMenu('A')
        jMenu.addAction(self.lolAct)

        slidesMenu.addAction(self.newSlideAct)
        slidesMenu.addAction(self.dupAct)
        slidesMenu.addAction(self.delAct)
        slidesMenu.addSeparator()
        slidesMenu.addAction(self.nextAct)
        slidesMenu.addAction(self.prevAct)
        slidesMenu.addAction(self.lastAct)
        slidesMenu.addAction(self.firstAct)
        styleMenu.addAction(self.incSizeLargeAct)
        styleMenu.addAction(self.decSizeLargeAct)
        styleMenu.addAction(self.incSizeAct)
        styleMenu.addAction(self.decSizeAct)
        styleMenu.addSeparator()
        styleMenu.addAction(self.incmargAct)
        styleMenu.addAction(self.decmargAct)
        styleMenu.addSeparator()
        styleMenu.addAction(self.colorAct)
        styleMenu.addAction(self.progAct)
        styleMenu.addAction(self.titleAct)
        styleMenu.addSeparator()
        styleMenu.addAction(self.hflipAct)
        styleMenu.addAction(self.vflipAct)
        styleMenu.addSeparator()
        styleMenu.addAction(self.maximizeAct)
        styleMenu.addAction(self.settingsAct)
        helpMenu.addAction(self.helpAct)

    def setActions(self):
        """ creates QActions for the menu bar
            and links them to appropriate functions and shortcuts """
        # New Slide
        self.newSlideAct = QAction("New Slide", controller)
        self.newSlideAct.setShortcuts(QKeySequence.New)
        self.newSlideAct.setStatusTip("Create a new slide")
        self.newSlideAct.triggered.connect(tempnewslide)
        self.newSlideAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.newSlideAct)

        # Duplicate
        self.dupAct = QAction("Duplicate Slide", controller)
        self.dupAct.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_D))
        self.dupAct.setStatusTip("Duplicate current slide")
        self.dupAct.triggered.connect(slides.duplicate)
        self.dupAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.dupAct)

        # new Document
        self.newAct = QAction("New Document", controller)
        self.newAct.setShortcut(QKeySequence(Qt.CTRL + Qt.SHIFT + Qt.Key_N))
        self.newAct.setStatusTip("Create new document")
        self.newAct.triggered.connect(newDocument)
        self.newAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.newAct)

        # Save
        self.saveAct = QAction("Save As...", controller)
        self.saveAct.setShortcuts(QKeySequence.Save)
        self.saveAct.setStatusTip("Save current document")
        self.saveAct.triggered.connect(io.save)
        self.saveAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.saveAct)

        # Open
        self.openAct = QAction("Open...", controller)
        self.openAct.setShortcuts(QKeySequence.Open)
        self.openAct.setStatusTip("Open a document")
        self.openAct.triggered.connect(io.openFile)
        self.openAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.openAct)

        # Delete
        self.delAct = QAction("Delete Slide", controller)
        self.delAct.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_Backspace))
        self.delAct.setStatusTip("Delete Current Slide")
        self.delAct.triggered.connect(slides.delActive)
        self.delAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.delAct)

        # Next
        self.nextAct = QAction("Next Slide", controller)
        self.nextAct.setShortcut(QKeySequence(Qt.Key_Right))
        self.nextAct.setStatusTip("Go to the next slide")
        self.nextAct.triggered.connect(slides.nextSlide)
        self.nextAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.nextAct)

        # Previous
        self.prevAct = QAction("Previous Slide", controller)
        self.prevAct.setShortcut(QKeySequence(Qt.Key_Left))
        self.prevAct.setStatusTip("Go to the previous slide")
        self.prevAct.triggered.connect(slides.prevSlide)
        self.prevAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.prevAct)

        # Last
        self.lastAct = QAction("Last Slide", controller)
        self.lastAct.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_Right))
        self.lastAct.setStatusTip("Go to the last slide")
        self.lastAct.triggered.connect(slides.setLast)
        self.lastAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.lastAct)

        # First Slide
        self.firstAct = QAction("First Slide", controller)
        self.firstAct.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_Left))
        self.firstAct.setStatusTip("Go to the first slide")
        self.firstAct.triggered.connect(slides.setFirst)
        self.firstAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.firstAct)

        # Increase Font Size
        self.incSizeLargeAct = QAction("Increase Font Size", controller)
        self.incSizeLargeAct.setShortcut(QKeySequence(Qt.Key_Up))
        self.incSizeLargeAct.setStatusTip("Increase font size")
        self.incSizeLargeAct.triggered.connect(incFontLarge)
        self.incSizeLargeAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.incSizeLargeAct)

        # Decrease Font Size
        self.decSizeLargeAct = QAction("Decrease Font Size", controller)
        self.decSizeLargeAct.setShortcut(QKeySequence(Qt.Key_Down))
        self.decSizeLargeAct.setStatusTip("Decrease Font Size")
        self.decSizeLargeAct.triggered.connect(decFontLarge)
        self.decSizeLargeAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.decSizeLargeAct)

        # Finely Increase Font Size
        self.incSizeAct = QAction("Finely Increase Font Size", controller)
        self.incSizeAct.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_Up))
        self.incSizeAct.setStatusTip("Finely Increase font size")
        self.incSizeAct.triggered.connect(incFont)
        self.incSizeAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.incSizeLargeAct)

        # Finely Dec Font Size
        self.decSizeAct = QAction("Finely Decrease Font Size", controller)
        self.decSizeAct.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_Down))
        self.decSizeAct.setStatusTip("Finely Decrease Font Size")
        self.decSizeAct.triggered.connect(decFont)
        self.decSizeAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.decSizeAct)

        # Color
        self.colorAct = QAction("Color...", controller)
        self.colorAct.setShortcut(QKeySequence(Qt.CTRL + Qt.SHIFT + Qt.Key_C))
        self.colorAct.setStatusTip("Set Text Color")
        self.colorAct.triggered.connect(chooseColor)
        self.colorAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.colorAct)

        # Finely Dec Font Size
        self.maximizeAct = QAction("Fill Screen", controller)
        self.maximizeAct.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_F))
        self.maximizeAct.setStatusTip("Maximize teleprompting window")
        self.maximizeAct.triggered.connect(fillScreen)
        self.maximizeAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.maximizeAct)

        # increase Margins
        self.incmargAct = QAction("Increase Margins", controller)
        self.incmargAct.setShortcut(QKeySequence(Qt.ALT + Qt.Key_Up))
        self.incmargAct.setStatusTip("Increase Viewer Margins")
        self.incmargAct.triggered.connect(incMargins)
        self.incmargAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.incmargAct)

        # decrease Margins
        self.decmargAct = QAction("Decrease Margins", controller)
        self.decmargAct.setShortcut(QKeySequence(Qt.ALT + Qt.Key_Down))
        self.decmargAct.setStatusTip("Decrease Viewer Margins")
        self.decmargAct.triggered.connect(decMargins)
        self.decmargAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.decmargAct)

        # help
        self.helpAct = QAction("A moose once bit my sister...", controller)
        self.helpAct.setStatusTip("MUAHAHAHAHA")
        self.helpAct.triggered.connect(nothing)
        self.helpAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.helpAct)

        # show progress indicator
        self.progAct = QAction("Show Progress", controller)
        self.progAct.setStatusTip("Whether or not to show progress indicator")
        self.progAct.setCheckable(True)
        self.progAct.setChecked(options.showProgressIndicator)
        self.progAct.triggered.connect(showProgress)
        self.progAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.progAct)

        # show title
        self.titleAct = QAction("Show Title", controller)
        self.titleAct.setStatusTip("Shows a slide's title in the viewer")
        self.titleAct.setCheckable(True)
        self.titleAct.setChecked(options.showTitle)
        self.titleAct.triggered.connect(showTitle)
        self.titleAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.titleAct)

        # more settings
        self.settingsAct = QAction("Settings...", controller)
        self.settingsAct.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_Comma))
        self.settingsAct.setStatusTip("Change slide settings")
        self.settingsAct.triggered.connect(openSettings)
        self.settingsAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.settingsAct)

        # horizontal flip
        self.hflipAct = QAction("Horizontal Flip", controller)
        self.hflipAct.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_I))
        self.hflipAct.setStatusTip("Flips viewer horizontally")
        self.hflipAct.setCheckable(True)
        self.hflipAct.setChecked(options.hflip)
        self.hflipAct.toggled.connect(hflipCheck)
        self.hflipAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.hflipAct)

        # vertical flip
        self.vflipAct = QAction("Vertical Flip", controller)
        self.vflipAct.setShortcut(QKeySequence(Qt.CTRL + Qt.Key_O))
        self.vflipAct.setStatusTip("Flips viewer vertically")
        self.vflipAct.setCheckable(True)
        self.vflipAct.setChecked(options.vflip)
        self.vflipAct.toggled.connect(vflipCheck)
        self.vflipAct.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.vflipAct)

        # lol
        self.lolAct = QAction("lol", controller)
        self.lolAct.triggered.connect(nothing)
        controller.addAction(self.lolAct)
"""
        #
        self.Act = QAction("", controller)
        self.Act.setShortcut(QKeySequence())
        self.Act.setStatusTip("")
        self.Act.triggered.connect()
        self.Act.setShortcutContext(Qt.ApplicationShortcut)
        controller.addAction(self.Act)
"""


class SettingsWindow(QDialog):
    """ creates a small window of options (preferences)
        that aren't necessary to change constantly """
    def __init__(self, parent=None):
        super(SettingsWindow, self).__init__(parent)
        self.setGeometry(500, 320, 280, 320)
        self.setWindowTitle("thePrompter -- Settings")
        self.createbuttons()

    def createbuttons(self):
        """ create and connect buttons and sliders in the settings window """
        #color:
        self.colorselectbut = QPushButton("Set color")
        self.colorselectbut.clicked.connect(chooseColor)
        #flips:
        self.hflip = QCheckBox()
        if options.hflip == True:
            self.hflip.toggle()
        self.hflip.toggled.connect(hflipCheck)
        if options.vflip == True:
            self.vflip.toggle()
        self.vflip = QCheckBox()
        self.vflip.toggled.connect(vflipCheck)
        #bold ital und
        self.bold = QCheckBox()
        self.bold.toggle()
        self.bold.toggled.connect(boldChecked)
        self.ital = QCheckBox()
        self.ital.toggled.connect(italChecked)
        self.und = QCheckBox()
        self.und.toggled.connect(undChecked)
        # top margin
        self.topmargin = QSlider()
        self.topmargin.setRange(0, 100)
        self.topmargin.setSliderPosition(int(options.vmarginMult * 100))
        self.topmargin.setOrientation(Qt.Horizontal)
        self.topmargin.setMaximumWidth(140)
        self.topmargin.setMinimumWidth(140)
        self.topmargin.valueChanged.connect(setTopMargin)
        ##############
        self.optionslayout = QFormLayout()
        self.optionslayout.setAlignment(Qt.AlignTop)
        self.optionslayout.addRow(self.tr("Color:"), self.colorselectbut)
        self.optionslayout.addRow(self.tr("Bold:"), self.bold)
        self.optionslayout.addRow(self.tr("Italics:"), self.ital)
        self.optionslayout.addRow(self.tr("Underline:"), self.und)
        self.optionslayout.addRow(self.tr("Horizontal Flip:"),  self.hflip)
        self.optionslayout.addRow(self.tr("Vertical Flip:"),  self.vflip)
        self.optionslayout.addRow(self.tr("Top Margin: "),  self.topmargin)

        self.confirm = QPushButton("Done")
        self.confirm.setMaximumWidth(100)
        self.confirm.clicked.connect(self.closeWindow)

        self.layout = QVBoxLayout()
        self.layout.addLayout(self.optionslayout)
        self.layout.addWidget(self.confirm)
        self.setLayout(self.layout)

    def closeWindow(self):
        """ closes window """
        update()
        self.close()


class MainWindow(QMainWindow):
    """ creates a main window which holds the controller.
        perhaps this doesn't need its own class. oh well"""
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setGeometry(960, 100, 720, 680)
        self.setWindowTitle("thePrompter -- Controller")
        self.setMinimumSize(720, 680)
        self.setMaximumWidth(720)
        self.setCentralWidget(controller)

    def closeEvent(self, event):
        """ prompt you to save on a window close or application close event
            if you haven't saved yet, a save dialog appears,
            otherwise the save operation is called """
        if io.savedYet == False:
            s = askSave()
        else:
            s = 1  # if you've saved before, don't prompt, just save and quit
        if s is 1:
            io.save()
            event.ignore()  # ignore single window close then exit app
            sys.exit()
        elif s is 0:
            event.ignore()
            sys.exit()
        elif s is -1:
            event.ignore()


class Controller(QWidget):
    """ creates window allowing user to input text, change options,
        and manipulate slides """
    def __init__(self,  parent=None):
        super(Controller,  self).__init__(parent)

        self.setGeometry(960, 100, 540, 680)
        self.setLayouts()

    def updateList(self, title, text):
        """ updates every entry in the list of slides.
            displays the slide number,
            12 chars of the title, and 20 chars of the body """
        try:
            for i in range(0, self.slidelist.count()):
                item = self.slidelist.item(i)
                if i > slides.ilength():
                    self.slidelist.takeItem(i)
                    return
                itext = slides.text[i]
                ititle = trunc(slides.titles[i], 12)
                newtext = itext.replace("<br />", " ")
                newtext = trunc(newtext.replace("\n", " "), 20)
                item.setText("%s| %s: %s" % (str(i + 1), ititle, newtext))
            self.slidelist.setCurrentRow(slides.active)
        except:
            print "List update failed!"

    def setLayouts(self):
        """ creates all buttons and widgets and layouts for the controller """
        ######options and widgets#######
        #text entry area
        self.edit = QTextEdit("Text")
        self.edit.setMinimumWidth(350)
        #self.edit.setMaximumWidth(350)
        self.edit.selectAll()
        self.edit.setTabChangesFocus(True)
        self.edit.createStandardContextMenu()
        #title entry
        self.title = QLineEdit("Title")
        self.title.setMinimumWidth(175)
        self.title.setMaximumWidth(175)
        #preview
        self.preview = QGraphicsView(slides.currentScene())
        self.preview.setMaximumWidth(320)
        self.preview.setMaximumHeight(250)
        self.preview.setMinimumWidth(320)
        self.preview.setMinimumHeight(250)
        #size:
        self.sizebut = QSlider()
        self.sizebut.setRange(12, 120)
        self.sizebut.setSliderPosition(options.size)
        self.sizebut.setOrientation(Qt.Horizontal)
        self.sizebut.move(350, 250)
        self.sizebut.setMaximumWidth(140)
        self.sizebut.setMinimumWidth(140)
        self.sizebut.valueChanged.connect(setSize)
        #margins
        self.margins = QSlider()
        self.margins.setOrientation(Qt.Horizontal)
        self.margins.setRange(0, 150)
        self.margins.setSliderPosition(options.margins)
        self.margins.setMaximumWidth(140)
        self.margins.setMinimumWidth(140)
        self.margins.valueChanged.connect(changeMargins)
        #slides
        self.slidelayout = QVBoxLayout()
        self.slidelabel = QLabel("Slides")
        self.nextslide = QPushButton("->")
        self.prevslide = QPushButton("<-")
        self.newslide = QPushButton("New")
        self.delslide = QPushButton("Delete")
        self.openfile = QPushButton("Open...")
        self.savefile = QPushButton("Save...")
        self.settings = QPushButton("Settings...")
        self.prevslide.setMaximumWidth(90)
        self.nextslide.setMaximumWidth(90)
        self.newslide.setMaximumWidth(90)
        self.delslide.setMaximumWidth(90)
        self.openfile.setMaximumWidth(90)
        self.savefile.setMaximumWidth(90)
        self.settings.setMaximumWidth(90)
        self.prevslide.clicked.connect(slides.prevSlide)
        self.nextslide.clicked.connect(slides.nextSlide)
        self.newslide.clicked.connect(tempnewslide)
        self.delslide.clicked.connect(slides.delActive)
        self.openfile.clicked.connect(io.openFile)
        self.savefile.clicked.connect(io.save)
        self.settings.clicked.connect(openSettings)
        self.slidebuttons = QGridLayout()
        self.slidebuttons.addWidget(self.prevslide, 0, 0)
        self.slidebuttons.addWidget(self.nextslide, 0, 1)
        self.slidebuttons.addWidget(self.newslide, 0, 2)
        self.slidebuttons.addWidget(self.delslide, 1, 0)
        self.slidebuttons.addWidget(self.openfile, 1, 1)
        self.slidebuttons.addWidget(self.settings, 1, 2)

        self.sizeandmargins = QFormLayout()
        self.sizeandmargins.setHorizontalSpacing(10)
        self.sizeandmargins.addRow("Margins: ", self.margins)
        self.sizeandmargins.addRow("Size: ", self.sizebut)

        self.slidelist = QListWidget()
        self.slidelist.setMaximumWidth(320)
        self.slidelist.setMinimumWidth(320)
        self.slidelist.itemSelectionChanged.connect(slidelistchanged)
        self.slidelayout.addWidget(self.slidelabel)
        self.slidelayout.addLayout(self.sizeandmargins)
        self.slidelayout.addWidget(self.slidelist)
        self.slidelayout.addLayout(self.slidebuttons)

        self.fulllayout = QBoxLayout(QBoxLayout.LeftToRight)
        self.entrylayout = QVBoxLayout()
        self.optionslayout = QFormLayout()
        self.previewandoptions = QVBoxLayout()

        self.entrylayout.addWidget(self.title)
        self.entrylayout.addWidget(self.edit)

        self.previewandoptions.addWidget(self.preview)
        self.previewandoptions.addLayout(self.slidelayout)
        self.previewandoptions.setAlignment(Qt.AlignTop)

        self.fulllayout.addLayout(self.entrylayout)
        self.fulllayout.addLayout(self.previewandoptions)

        self.setLayout(self.fulllayout)

        self.edit.setFocus()
        self.edit.textChanged.connect(changeText)
        self.title.textChanged.connect(changeTitle)


class Viewer(QGraphicsView):
    """ creates window that is mean to actually teleprompt """
    def __init__(self, parent=None):
        super(Viewer, self).__init__(parent)
        self.setGeometry(0, 100, options.width, options.height)
        self.setScene(slides.currentScene())
        self.setViewportUpdateMode(QGraphicsView.MinimalViewportUpdate)
        self.window().setWindowTitle("thePrompter -- Viewer")

    def resizeEvent(self, event):
        """ calculates new scale used to ensure the graphics view
            shows the entire text properly zoomed in """
        size = event.size()
        scale = float(size.width()) / options.width
        options.zoomX = scale
        options.zoomY = options.zoomX
        updateTransform()

    def closeEvent(self, event):
        """ links viewer being closed to controller being closed """
        controller.closeEvent(event)


class IO:
    """ handles all file opening and file saving """
    def __init__(self):
        self.savepath = ""
        self.savedYet = False

    def write(self, filename):
        """ write your document as a .txt file to <filename> using this format:
            >> TITLE 1
            >> BODY1 BODYBODYBODYBODYBODY<br />forwhenyouwantanewlineBODYBODY
            >>
            >>
            >> TITLE 2 etc.
        """
        try:
            f = open(filename, 'w')
            for e in range(0, slides.text.__len__()):
                f.write(slides.titles[e])
                f.write("\n")  # title has its own line
                f.write(slides.text[e])
                f.write("\n\n\n")  # body then two blank lines
            f.close()
        except:
            "error writing file!"

    def read(self, filename):
        """ reads a file and calls getSlides to format the file as slides """
        try:
            f = open(filename, 'r')
            newDocument()
            self.getSlides(f)
            slides.delete(0)
            f.close()
            update()
        except:
            print "error reading file!"

    def save(self):
        """ ask where to save your file.
            prompt for save destination to save to first time,
            then doesn't ask anymore """
        if self.savedYet is False:
            self.savepath = self.getSaveDestination()
            self.saveYet = True
        if self.savepath[4:] != ".txt":
            self.savepath += ".txt"
        self.write(self.savepath)
        self.savedYet = True

    def saveAs(self):
        """ like save(), but will always prompt for save destination """
        savepath = self.getSaveDestination()
        self.write(savepath)

    def openFile(self):
        """ performs the file opening when about to read a file """
        f = self.getFileToOpen()
        self.read(f)

    def getSaveDestination(self):
        """ prompts user for where to save file and its name """
        destination = QFileDialog.getSaveFileName()
        return destination[0]  # just need the path

    def getFileToOpen(self):
        """ prompts user for which file to open """
        destination = QFileDialog.getOpenFileName()
        return destination[0]

    def getSlides(self, f):
        """ parses read file.
            not sure that it really needs to be a separate method... """
        temptitle = ""
        tempbody = ""
        for i, line in enumerate(f):
            if i % 4 == 0:
                temptitle = line.replace("\n", "")
            elif i % 4 == 1:
                tempbody = line
                slides.new(temptitle, tempbody)


def askSave():
    """ creates save dialog """
    msgBox = QMessageBox()
    msgBox.setText("The document has been modified")
    msgBox.setInformativeText("Do you want to save changes?")
    msgBox.setStandardButtons(\
        QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
    msgBox.setDefaultButton(QMessageBox.Save)
    msgBox.setEscapeButton(QMessageBox.Discard)
    msgBox.setIcon(QMessageBox.Question)
    ret = msgBox.exec_()
    if ret == QMessageBox.Save:
        return 1
    elif ret == QMessageBox.Discard:
        return 0
    elif ret == QMessageBox.Cancel:
        return -1


def nothing():
    pass  # YOLO


def openSettings():
    """ opens new instance of settings window """
    global settingsWindow
    settingsWindow = SettingsWindow()
    settingsWindow.show()


def newDocument():
    """ deletes every slide except the first,
        but overwrites the first with the default text """
    for i in range(0, slides.ilength()):
        slides.delete(0)
    controller.edit.setText(defaultnewtext)
    controller.title.setText(defaultnewtitle)


def fillScreen():
    """ makes viewer window be maximized
        and minimized again if called twice """
    if view.isMaximized() == True:
        view.setGeometry(0, 100, options.width, options.height)
    else:
        view.showMaximized()
        view.scroll(0, -100)


def showTitle():
    """ sets whether the title is shown above slides """
    options.showTitle = not options.showTitle
    menu.titleAct.setChecked(options.showTitle)
    update()


def showProgress():
    """ sets whether "1 of 2" is shown above slides """
    options.showProgressIndicator = not options.showProgressIndicator
    menu.progAct.setChecked(options.showProgressIndicator)
    update()


def incFont():
    controller.sizebut.setValue(controller.sizebut.value() + 1)


def incFontLarge():
    controller.sizebut.setValue(controller.sizebut.value() + 5)


def decFont():
    controller.sizebut.setValue(controller.sizebut.value() - 1)


def decFontLarge():
    controller.sizebut.setValue(controller.sizebut.value() - 5)


def incMargins():
    controller.margins.setValue(controller.margins.value() + 5)


def decMargins():
    controller.margins.setValue(controller.margins.value() - 5)


def slidelistchanged():
    """ if a slide is selected from the slide list,
        that slide is set as active slide """
    if options.updateOnEdit is False:
        return
    slide = controller.slidelist.currentRow()
    slides.changeActive(slide)


def changeText():
    """ called whenever the entered text is changed. simply calls update() """
    if options.updateOnEdit is True:
        update()


def changeTitle():
    """ called whenever the entered title is changed.
        also just calls update() """
    if options.updateOnEdit is True:
        update()


def tempnewslide():
    """ called when new slide button is pushed,
        or if the menu bar 'new' is used """
    slides.new(defaultnewtitle, defaultnewtext)


def setTopMargin(margin):
    options.vmarginMult = margin / 100.0
    update()


def boldChecked(state):
    options.bold = state
    update()


def italChecked(state):
    options.ital = state
    update()


def undChecked(state):
    options.und = state
    update()


def changeMargins():
    """ called when margin slider is changed
        updates the display to have the correct value """
    try:
        options.margins = controller.margins.value()
    except:
        print "margin change failed"
    update()


def hflipCheck(state):
    options.hflip = state
    menu.hflipAct.setChecked(state)
    updateTransform()


def vflipCheck(state):
    options.vflip = state
    menu.vflipAct.setChecked(state)
    updateTransform()


def chooseColor():
    """ prompts user to select a color,
        then converts it to hex and save that value for the text color """
    colorselect = QColorDialog
    color = colorselect.getColor()
    if color.isValid() is False:
        return
    options.color = qColorToHex(color)
    update()


def setSize():
    """ set font size to be the value in the controller """
    options.size = controller.sizebut.value()
    update()


def setStyle():
    """ uses options in Options class to create css&html header for viewer """
    style = "<head><style> \np {\n"
    margins = "margin-left:%spx;" % (options.margins)
    margins += "\nmargin-right:%spx;\n" % (options.margins)

    checkoptions()
    if options.bold == True:
        boldtag = "font-weight:bold;\n"
    else:
        boldtag = ""
    if options.ital == True:
        italtag = "font-style:italic;\n"
    else:
        italtag = ""
    if options.und == True:
        undtag = "text-decoration:underline;\n"
    else:
        undtag = ""

    if options.size > 0:
        sizetag = "font-size:" + str(options.size) + "px;\n"

    colortag = "color:#" + options.color + ";\n"
    facetag = "font-family:" + options.face + ";\n"

    style = "<head><style> \np {\n" + margins + sizetag + "}"
    style += "\nh1 {\nfont-size:200%;\n}"
    style += "\np, h1 {\n" + colortag + facetag + boldtag + italtag + undtag
    style += "} </style></head>\n"
    return style


def checkoptions():
    """ ensure bold ital und and color don't have undefined settings """
    if options.bold == None:
        options.bold = defaultbold
    if options.ital == None:
        options.ital = defaultital
    if options.und == None:
        options.und = defaultund
    if options.color == None:
        options.color = defaultcolor


def qColorToHex(QColor):
    """ convert Qt's QColor to CSS's Hex format (#RRGGBB) :: QColor -> str """
    r, g, b, a = QColor.toTuple()
    return getHex(r) + getHex(g) + getHex(b)


def getHex(n):
    """ get hex string from number without '0x' """
    tens = n / 16
    ones = n % 16
    result = hex(tens)[2:] + hex(ones)[2:]
    return result


def update():
    """ update display's text with new text or new options """
    if slides.active is -1:
        if slides.isslide(0) is False:
            return  # will not update if no slides exist
        else:
            slides.changeActive(slides.ilength())
    slides.titles[slides.active] = controller.title.text()
    view.setScene(slides.currentScene())
    controller.preview.setScene(slides.currentScene())
    style = setStyle()
    enteredText = controller.edit.toPlainText()
    enteredText = enteredText.replace("\n", "<br />")  # replace \n with <br />
    if options.showTitle == True:
        title = controller.title.text()
    else:
        title = ""
    if options.showProgressIndicator == True:
        current = "%s of %s" % (slides.active + 1, slides.text.__len__())
        if title != "":
            current += ": "
    else:
        current = ""
    allText = style + "<body><h1><center>" + current + title
    allText += "</center></h1><p>" + enteredText + "</p></body>"
    slides.setCurrentText(title, enteredText, allText)
    controller.updateList(title, enteredText)
    updateTransform()


def updateTransform():
    """ update display's matrix transforms. handles flipping and resizing """
    if slides.active is -1:
        return
    if options.zoomX < 0.02:
        print "zoomX: %s out of bounds" % options.zoomX
        return

    if options.hflip is True:
        hflip = -1
    else:
        hflip = 1
    if options.vflip is True:
        vflip = -1
    else:
        vflip = 1

    sx = hflip * options.zoomX * 0.99
    sy = vflip * options.zoomY * 0.99
    slides.slides[slides.active].setY(options.margins * options.vmarginMult)
    mat = QMatrix(sx, 0, 0, sy, 0, 0)
    view.setMatrix(mat)
    px = 310.0 / options.width
    py = 240.0 / options.height
    controller.preview.setMatrix(QMatrix(px, 0, 0, py, 0, 0))


if __name__ == '__main__':
    print "running...\nversion %s" % (__version__)

    app = QApplication(sys.argv)

    options = Options()
    io = IO()
    slides = Slides()
    controller = Controller()
    view = Viewer()
    mainWindow = MainWindow()
    menu = Menu()

    print "Done Loading..."

    mainWindow.show()
    view.show()

    view.setFocus()
    controller.setFocus()

    slides.new("Title", placeholder)

    sys.exit(app.exec_())
